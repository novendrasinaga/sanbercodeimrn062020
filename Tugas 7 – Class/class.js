class Animal { 
	constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    } 
}

console.log('SOAL 1') 
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
 
class Frog extends Animal {
  constructor(name) {
    super(name);
    super.legs=2;
	this.lompat='hop hop';
  } 
  jump() {
    return  this.lompat;
  }
}

class Ape extends Animal {
  constructor(name) {
    super(name); 
	this.suara='Auooo';
  } 
  yell() {
    return  this.suara;
  }
}


console.log('\n\nSOAL 2') 
var sungokong = new Ape("kera sakti")
console.log(sungokong)
console.log(sungokong.yell()) // "Auooo"

console.log('\n') 

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok)
console.log(kodok.jump())



class Clock {
	//var timer;
	
	constructor({template}) {
		this.template = template;
		return this.template;
	} 
	
	
	render() {
		var date = new Date();

		var hours = date.getHours();
		if (hours < 10) hours = '0' + hours;

		var mins = date.getMinutes();
		if (mins < 10) mins = '0' + mins;

		var secs = date.getSeconds();
		if (secs < 10) secs = '0' + secs;

		//this.output = date
		  //.replace('h', hours)
		  //.replace('m', mins)
		  //.replace('s', secs);
		this.output = hours + ':' + mins + ':' + secs
		console.log(this.output);
	}
	
	stop()
	{
		clearInterval(this.timer);
	}
	
	start()
	{
		this.render();
		this.timer = setInterval(this.render, 1000);
	}
	 
	
}



console.log('\n\nSOAL 3') 
var clock = new Clock({template: 'h:m:s'});
clock.render();
clock.start();


  