console.log('SOAL 1');

function teriak()
{
	return 	'Halo Sanbers!';
}
 
console.log(teriak()) // "Halo Sanbers!" 


console.log('\n\nSOAL 2');

function kalikan(a,b)
{
	return a*b;
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);


console.log('\n\nSOAL 3');

function introduce(nama, age, address, hobby)
{
	return 'Nama Saya ' + nama + ', umur saya ' + age + ' Tahun, alamat saya ' + address + ', dan saya punya Hobby yaitu ' + hobby +'!';
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
 
 // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
