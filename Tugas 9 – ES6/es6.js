const cetak = (no) => console.log('\n\nSoal ' + no)
const cetakgaris = () => console.log('--------------------------------------')
 
cetak(1)
cetakgaris()
 
const golden = goldenFunction = () => console.log('this is golden!!')

golden()

cetak(2)
cetakgaris()

const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
      let namalengkap =  `${this.firstName} ${this.lastName}`
	  console.log(namalengkap)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 



cetak(3)
cetakgaris()

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation)



cetak(4)
cetakgaris()

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


cetak(5)
cetakgaris()


const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 

