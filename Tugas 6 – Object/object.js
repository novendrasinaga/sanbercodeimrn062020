function arrayToObject(arr) {
    // Code di sini 
	if(arr.length==0)
		console.log('""')
	else 
	{
		var Arrobj = [];
		arr.forEach((data) => {
			//console.log();
			
			var now = new Date()
			var thisYear = now.getFullYear() // 2020 (tahun sekarang)
			
			if(data[3]==undefined)
				strage= 'Invalid Birth Year'
			else 
			{
				strage= thisYear - data[3]
				if(strage<0)
					strage= 'Invalid Birth Year'
			}
			var obj = {
				firstName : data[0],
				lastName: data[1],
				gender: data[2],
				age: strage
			} 
			Arrobj.push(obj);	
		});
		
		var no = 0;
		Arrobj.forEach((data) => {
			no++; 
			var str = no + data
			console.log(no + ' . ' + data.firstName + ' ' + data.lastName + ' : ')
			console.log(data) 
		});
	
	} 
}
 
console.log('SOAL 1'); 
// Driver Code



var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

arrayToObject([]) // ""




console.log('\n\nSOAL2')

function shoppingTime(memberId, money) {
  // you can only write your code here!
	var sisa = money;
	var barang=[];
	
	if(memberId=='')
		return ('Mohon maaf, toko X hanya berlaku untuk member saja');
	else if(typeof memberId === 'undefined' || typeof money === 'undefined')
		return 'Mohon maaf, toko X hanya berlaku untuk member saja';
	else 
	{
		if(sisa>=1500000)
		{
			barang.push('Sepatu Stacattu');
			sisa-=1500000;
		}
		 
		if(sisa>=500000)
		{
			barang.push('Baju Zoro');
			sisa-=500000;
		}
		
		
		if(sisa>=250000)
		{
			barang.push('Baju H&N');
			sisa-=250000;
		}
		
		if(sisa>=175000)
		{
			barang.push('Sweater Uniklooh');
			sisa-=175000;
		}
		
		
		if(sisa>=50000)
		{
			barang.push('Casing Handphone');
			sisa-=50000;
		}
		
		if(barang.length==0)
		{
			return 'Mohon maaf, uang tidak cukup';
		}
		else 
		{
			var obj = {
			memberId : memberId,
			money: money,
			listPurchased: barang,
			changeMoney: sisa
			} 
			return obj;
		} 
	} 
}


console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());



console.log('\n\nSOAL3')

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  
	if(arrPenumpang.length>0)
	{
		var arrObj = [];
		for(i=0;i<arrPenumpang.length;i++)
		{ 
			var arr =  arrPenumpang[i];
			var awal = rute.indexOf(arr[1]);
			var akhir = rute.indexOf(arr[2]);
			var selisih = parseInt(akhir) - parseInt(awal);
			
			var obj = {
				penumpang : arr[0],
				naikDari: arr[1],
				tujuan: arr[2],
				bayar: (selisih*2000)
			} 
			
			arrObj.push(obj);
		}
		return arrObj;
	  
	}
	else 
	  return '[]';
 
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]





