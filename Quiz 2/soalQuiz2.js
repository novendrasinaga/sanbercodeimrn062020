class Score {
  // Code disini
  
  constructor(subject, points,email)
  {
		this.subject = subject;
		this.points = points;
		this.email = email;
  } 
   

  average()
  {
	  let sum = (this.points).reduce(function (accumulator, currentValue) {
		  return accumulator + currentValue
	  }, 0)
	  return sum;
 
  }
  
}

let nilai = [3,4,5,6];

const soal1 = new Score('Tugas Novendra Sinaga', nilai, 'mrnoven@gmail.com');
console.log(soal1);
console.log(`Nilai Rata-Rata : ${soal1.average()}`);



const viewScores = function viewScores(Arrdata, subject){
  const arrObj=[];
  let Arr = {}
  if(subject=='quiz-1')
  {
	for(i=1;i<Arrdata.length;i++)
	{
		let Arr = {}
		Arr.email = Arrdata[i][0];
		Arr.subject = 'quiz-1';
		Arr.points = Arrdata[i][1];
		arrObj.push(Arr);
		
	}		
  }
  else if(subject=='quiz-2')
  {
	for(i=1;i<Arrdata.length;i++)
	{
		let Arr = {}
		Arr.email = Arrdata[i][0];
		Arr.subject = 'quiz-2';
		Arr.points = Arrdata[i][2];
		arrObj.push(Arr); 
	}		
  }
  
  else if(subject=='quiz-3')
  {
	for(i=1;i<Arrdata.length;i++)
	{
		
		let Arr = {}
		Arr.email = Arrdata[i][0];
		Arr.subject = 'quiz-3';
		Arr.points = Arrdata[i][3];
		arrObj.push(Arr); 
	}		
  }
  
  
  
  console.log(arrObj)
  return ;
}


console.log('\n\n\n Soal 2')
const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
];

viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

function round(value, decimals) {
 return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}   

const recapScores = function recapScores(Arrdata){
	let no = -1;
	Arrdata.forEach((soal3) => {
		no++;
		if(no>0)
		{
			const rata = round((soal3[1]+soal3[2]+soal3[3])/3,1);
			let predikat = 'participant';
			if(rata>90)
				predikat = 'honour';
			else if(rata>80)
				predikat = 'graduate';
			
			
			console.log(`${no}. Email : ${soal3[0]}`)
			console.log(`Rata-Rata : ${rata}`)
			console.log(`Predikat : ${predikat}\n`)
		}
	});
}

console.log('\n\n\nSoal 3')
recapScores(data);




