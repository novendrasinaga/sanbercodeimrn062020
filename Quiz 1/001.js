function balikString(kata) {
	var kalimat = '';
	for(i=kata.length-1;i>=0;i--)
		kalimat += kata[i];
	return kalimat;
}

console.log('// TEST CASES BalikString')
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


function palindrome(input) {
  // Silakan tulis code kamu di sini
  
  var balik = balikString(input);
  return balik==input
}

console.log('\n\n// TEST CASES Palindrome')

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false


function bandingkan(num1, num2) {
	/*
	Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 
	*/
	
	if(typeof num1 === 'undefined' && typeof num2 === 'undefined')
		return (-1);
	if(typeof num1 != 'undefined'  && typeof num2 === 'undefined')
		return num1;
	
	if(num1<0 || num2<0)
		return -1;
	else if(num1==num2)
		return -1
	else 
	{ 
		if(num1>num2)
			return num1;
		else 
			return num2;
	}
	 
	
}

console.log('\n\n// TEST CASES Bandingkan Angka');
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
