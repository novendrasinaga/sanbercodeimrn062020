var nama = "Junaedi";
var peran = "Werewolf";
var kalimat1, kalimat2;
if (nama == '')
    console.log('Nama harus diisi!');
else if (peran == '')
    console.log("Halo John, Pilih peranmu untuk memulai game!");
else if (nama == 'Jane' && peran == 'Penyihir') {
    kalimat1 = 'Selamat datang di Dunia Werewolf, ' + nama;
    kalimat2 = 'Halo ' + peran + ' ' + nama + ',  kamu dapat melihat siapa yang menjadi werewolf!';
    console.log(kalimat1);
    console.log(kalimat2);
} else if (nama == 'Jenita' && peran == 'Guard') {
    kalimat1 = 'Selamat datang di Dunia Werewolf, ' + nama;
    kalimat2 = 'Halo ' + peran + ' ' + nama + ',  kamu akan membantu melindungi temanmu dari serangan werewolf';
    console.log(kalimat1);
    console.log(kalimat2);
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
    kalimat1 = 'Selamat datang di Dunia Werewolf, ' + nama;
    kalimat2 = 'Halo ' + peran + ' ' + nama + ',  Kamu akan memakan mangsa setiap malam!';
    console.log(kalimat1);
    console.log(kalimat2);
}





var tanggal = 31; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 12; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2200; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (true) {
    case (tanggal < 1 || tanggal > 31):
        console.log('Tanggal antara 1-31');
        break;
    case (bulan < 1 || bulan > 12):
        console.log('Bulan antara 1-12');
        break;
    case (tahun < 1900 || tahun > 2200):
        console.log('Tahun antara 1900 - 2200');
        break;
    default:
        var lblbulan = '';
        switch (bulan) {
            case 1:
                lblbulan = 'Januari';
                break;
            case 2:
                lblbulan = 'Februari';
                break;
            case 3:
                lblbulan = 'Maret';
                break;
            case 4:
                lblbulan = 'April';
                break;
            case 5:
                lblbulan = 'Mei';
                break;
            case 6:
                lblbulan = 'Juni';
                break;
            case 7:
                lblbulan = 'Juli';
                break;
            case 8:
                lblbulan = 'Agustus';
                break;
            case 9:
                lblbulan = 'September';
                break;
            case 10:
                lblbulan = 'Oktober';
                break;
            case 11:
                lblbulan = 'November';
                break;
            default:
                lblbulan = 'Desember';
        }

        console.log(tanggal + ' ' + lblbulan + ' ' + tahun);
}