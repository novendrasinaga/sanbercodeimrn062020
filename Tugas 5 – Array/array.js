function range(startNum, finishNum) 
{ 
	if(typeof startNum === 'undefined' || typeof finishNum === 'undefined')
		return (-1);
	else 
	{
		var i, n ; 
		var numbers = [];
		
		if(startNum<finishNum)
		{
			for(no = startNum ; no<=finishNum; no++)
			{
				numbers.push(no);	
			}
		}
		else 
		{
			for(no = startNum ; no>=finishNum; no--)
			{
				numbers.push(no);	
			}
		}
		 
		return numbers;	
		
	}	
}

 
console.log('SOAL 1');
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


console.log('\n\nSOAL 2');


function rangeWithStep(startNum, finishNum, step) 
{
	if(typeof startNum === 'undefined' || typeof finishNum === 'undefined')
		return (-1);
	else 
	{
		var i, n ; 
		var numbers = [];
		
		if(startNum<finishNum)
		{
			for(no = startNum ; no<=finishNum; no+=step)
			{
				numbers.push(no);	
			}
		}
		else 
		{
			for(no = startNum ; no>=finishNum; no-=step)
			{
				numbers.push(no);	
			}
		}
		 
		return numbers;	
		
	}	
}



console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 



console.log('\n\nSOAL 3');

function sum(startNum, finishNum, step) 
{
	var numbers = [];
	if(typeof startNum === 'undefined' && typeof finishNum === 'undefined' && typeof step === 'undefined')
	{
		return 0;
	}
	else	
	{
		if(typeof startNum !== 'undefined' && typeof finishNum !== 'undefined' && typeof step !== 'undefined')
			numbers = rangeWithStep(startNum, finishNum, step);
		else if(typeof startNum !== 'undefined' && typeof finishNum !== 'undefined')
			numbers = range(startNum, finishNum);
		else 
			return startNum;
		
		
		var jlh = 0;
		
		numbers.forEach((data) => {
            jlh+=data;
        });
		
		return jlh;
			
	}		 
}
 
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log('\n\nSOAL 4');
 

function dataHandling(input)
{
	console.log(input[0].length);
	var title = ["Nomor ID", "Nama Lengkap", "TTL", "Hobby"];
	for(i=0;i<input.length;i++)
	{
		for(j=0;j<title.length;j++)
		{
			if(j<2)
				console.log(title[j] + ' : ' + input[i][j]);
			else if(j==2)
				console.log(title[j] + ' : ' + input[i][j] + ' ' + input[i][j+1]);
			else 
				console.log(title[j] + ' : ' + input[i][j+1]);
		}
		console.log('\n');
	}
}

var input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input);


console.log('\n\nSOAL 5');

function balikKata(kata)
{
	var kalimat = '';
	for(i=kata.length-1;i>=0;i--)
		kalimat += kata[i];
	return kalimat;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log('\n\nSOAL 6');

function dataHandling2(input)
{
	//array.splice([IndexMulai], [JumlahNilaiYangDihapus], [NilaiYangDitambahkan1], [NilaiYangDitambahkan2], ...);
	input.splice(1,1, 'Roman Alamsyah Elsharawy');
	input.splice(2,1, 'Provinsi Bandar Lampung');
	input.splice(4,1, 'Pria', 'SMA Internasional Metro')
	console.log(input);
	var tanggal = input[3];
	var pecah = tanggal.split('/');
	var bulan = '';
	switch(pecah[1])
	{
		case '01' : bulan ='Januari'; break;
		case '02' : bulan ='Februari'; break;
		case '03' : bulan ='Maret'; break;
		case '04' : bulan ='April'; break;
		case '05' : bulan ='Mei'; break;
		case '06' : bulan ='Juni'; break;
		case '07' : bulan ='Juli'; break;
		case '08' : bulan ='Agustus'; break;
		case '09' : bulan ='September'; break;
		case '10' : bulan ='Oktober'; break;
		case '11' : bulan ='November'; break;
		default : bulan ='Desember';
	}
	console.log(bulan);
	pecah.sort(function(a,b)
		{
			return b.length - a.length;
		}
	);
	console.log(pecah)	
	
	var pecah = tanggal.split('/');
	var slug = pecah.join("-")
	console.log(slug);
	
	var nama = input[1].slice(0,15) ;
	console.log(nama)
}
 
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 

