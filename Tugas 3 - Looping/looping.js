console.log('SOAL 1');
console.log('LOOPING PERTAMA');
var n = 10;
var i = 1;
while(i<=n)
{
	var isi = (i*2) + ' - I Love Cooding';
	console.log(isi);
	i++;
}


console.log('LOOPING KEDUA');
var i=10;
while(i>=1)
{
	var isi = (i*2) + ' - I will become a mobile developer';
	console.log(isi);
	i--;
}


console.log('\n\nSOAL 2');

var n = 20;
for(var i = 1; i<=n; i++)
{
	if(i%2==1)
	{
		if(i%3==0)
			var keterangan = i + ' - I Love Cooding';
		else 
			var keterangan = i + ' - Santai';
	}
	else 
		var keterangan = i + ' - Berkualitas';
	
	console.log(keterangan);
}



console.log('\n\nSOAL 3');
var baris = 4;
var kolom = 8;
for(i=1;i<=baris;i++)
{
	var keterangan='';
	for(j=1;j<=kolom;j++)
	{
		keterangan+='#';
	}
	console.log(keterangan);
}


console.log('\n\nSOAL 4');
var baris=7;
for(i=1;i<=baris;i++)
{
	var keterangan='';
	for(j=1;j<=i;j++)
	{
		keterangan+='#';
	}
	console.log(keterangan);
}

console.log('\n\nSOAL 5');
var ukuran = 8;
for(i=1;i<=ukuran;i++)
{
	var keterangan='';
	for(j=1;j<=ukuran;j++)
	{
		if(i%2==1)
		{
			if(j%2==1)
				keterangan+=' ';
			else 
				keterangan+='#';
		}
		else 
		{
			if(j%2==0)
				keterangan+=' ';
			else 
				keterangan+='#';
		}
	}
	console.log(keterangan);
}