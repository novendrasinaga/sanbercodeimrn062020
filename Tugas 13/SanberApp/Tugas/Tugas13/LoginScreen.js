import React, {} from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';

export default function LoginScreen() {

    return (
        <View style={styles.container}>
            <Image source={require('./images/logo-sanber.png')} style={styles.posisilogo} />
            <Text style={styles.textlogin}>Login</Text>
			
            <View style={styles.typeText}>
                <MaterialIcons style={{ marginRight: 15 }} name="mail" size={24} color="black" />
                <TextInput placeholder="Enter user / email" style={styles.textbox}></TextInput>
            </View>
			
            <View style={styles.typeText}>
                <MaterialIcons style={{ marginRight: 5 }} name="lock" size={24} color="black" />
                <TextInput placeholder="Enter password" style={styles.textbox}></TextInput>
            </View>
			
			<View style={styles.barisBaru}>
				<TouchableOpacity>
					<Text style={{marginLeft:100, marginTop:15}}>Lupa Password</Text>
				</TouchableOpacity> 
			</View>
           
			 
			
			<View style={styles.barisBaru}>
				<TouchableOpacity style={styles.button1}>
					<Text style={{color: "white"}}>Masuk</Text>
				</TouchableOpacity>
				 
				<TouchableOpacity style={styles.button2}>
					<Text style={{color: "white"}}>Daftar</Text> 
				</TouchableOpacity>
			</View>
			
			
			
			
        </View>

    );

}

const styles = StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
	textbox:
	{
		fontSize: 13
	},
	barisBaru: {
		flexDirection: 'row'
	},
	posisilogo:
	{
		width: 300, 
		height: 100, 
		marginTop: -100
	},
	textlogin:
	{
		marginTop: 25, 
		fontSize: 25
	},
    text: {
        fontSize: 30, color: '#04063c'
    },
    typeText: {
        marginTop: 25,
        width: 200,
        height: 30,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: "#003366",
        flexDirection: "row"
    },
    button1: {
        marginTop: 15,  
        height: 30,
        alignItems: 'center',
        backgroundColor: "#3EC6FF",
        paddingHorizontal: 25,
        paddingVertical: 5,
		marginRight:5,
        borderRadius: 200
    },
    button2: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
        backgroundColor: "#003366",
        paddingHorizontal: 25, 
        paddingVertical: 5, 
        borderRadius: 200,
		marginLeft:5
    }
});
