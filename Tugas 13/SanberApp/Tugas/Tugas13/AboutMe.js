import React from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    Image,
	ProgressBarAndroid
} from 'react-native';
 

export default function AboutScreen() {

    return (
        <View style={styles.container}>
			<View style={styles.bgAbout}>
				<Text style={styles.aboutme}>Tentang Saya</Text>
				
				<Image source={require('./images/novendra.jpg')} style={{ width: 100, height: 100, borderRadius: 300, marginTop: 10 }} />
				
				<Text style={styles.textaboutme}>Novendra Sinaga</Text>
            </View>
			
            <View style={styles.bgmedsos}> 
                <View style={styles.listmedsos}>
                      
						<FontAwesome name="facebook-square" size={34} color="#FFF" />
						
						<FontAwesome name="twitter-square" size={34} color="#FFF" /> 
						
						<FontAwesome name="instagram" size={34} color="#FFF" /> 
						
						<FontAwesome name="youtube-square" size={34} color="#FFF" /> 
						
						<FontAwesome name="envelope-o" size={34} color="#FFF" /> 
                </View>
            </View>
			
			
			<View style={styles.bgContact}> 
				<Text style={{fontSize:20, fontWeight: "bold", color:'blue'}}>Hubungi Saya</Text>
				<ProgressBarAndroid styleAttr="Horizontal" indeterminate={false} progress={1}/>	
				
				<View style={styles.listKontak}>
                    <View style={styles.detailKontak}>
                        <FontAwesome name="phone-square" size={34} color="#000" />
                        <Text style={styles.textcontact}>+62 85270434499</Text>
                    </View>
				</View>
				
				
				<View style={styles.listKontak}>
                    <View style={styles.detailKontak}>
                        <FontAwesome name="gitlab" size={34} color="#000" />
                        <Text style={styles.textcontact}>novendrasinaga</Text>
                    </View>
				</View>
				
				
				<View style={styles.listKontak}>
                    <View style={styles.detailKontak}>
                        <FontAwesome name="github" size={34} color="#000" />
                        <Text style={styles.textcontact}>novendrasinaga</Text>
                    </View>
				</View>
				
				
				<View style={styles.listKontak}>
                    <View style={styles.detailKontak}>
                        <FontAwesome name="location-arrow" size={34} color="#000" />
                        <Text style={styles.textcontact}>
						Jalan Mufakat Kiri no 108 {"\n"}
						Parluasan{"\n"}
						Pematangsiantar{"\n"}
						Sumatera Utara
						</Text>
						
                    </View>
				</View>
				
				 
				
				 
			</View>
			 
			
        </View>
    );
}

const styles = StyleSheet.create({
    container: { 
		marginTop:30,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
	bgContact:
	{ 
		 width:260,
	},
	
	textcontact:
	{
		marginLeft: 13, 
		marginTop:3,
		fontSize:17,
		
	},
	
	listKontak: {
        display: 'flex', 
    },
    detailKontak: {
		marginLeft:12,
        flexDirection: 'row',
        marginVertical: 6
    },
	bgAbout:
	{
		 backgroundColor: 'skyblue',
		 width:360,
		 height:250,
		 alignItems: 'center',
	},
	aboutme:
	{
		marginTop: 15, 
		fontSize: 30, 
		color: "#FFF", 
		marginTop: 0, 
		fontWeight: "bold"
	},
	textaboutme:
	{ 
		marginTop: 6, 
		fontSize: 25, 
		color: "#FFF", 
		fontWeight: 'bold'
	},
    bgmedsos: {
        display: 'flex',
        backgroundColor: "blue",
        height: 70,
        width: 320,
        marginBottom: 10,
        borderRadius: 10,
        marginTop: -40
    },
     
      
    detailPortofolio: {
        
        alignItems: 'center',
        marginTop: 10
    },
    listmedsos: {
		marginTop:14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});
