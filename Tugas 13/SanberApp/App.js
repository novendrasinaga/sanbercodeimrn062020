import {
  StatusBar
} from 'expo-status-bar';
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput
} from 'react-native';

// import untuk Soal Tugas 12
import Youtube from './Tugas/Tugas12/App'


// Import untuk Soal Tugas 13
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import RegisterScreen from './Tugas/Tugas13/RegisterScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import AboutMe from './Tugas/Tugas13/AboutMe'

// Import untuk Soal Tugas 14

import Tugas14 from './Tugas/Tugas14/App'
import Tugas14Skill from './Tugas/Tugas14/SkillScreen'

// Import untuk Soal Tugas 15
import Tugas15 from './Tugas/Tugas15/index'
import TugasNavigation from './Tugas/TugasNavigation/index'


// Quis3
import Quis3 from './Tugas/Quiz3/index';



export default function App() {
  return (
	/* Soal Tugas 12*/
    // <Youtube />
	
	/* Soal Tugas 13*/
    //<LoginScreen />
    //<RegisterScreen / > 
	//<AboutMe />
	
	/*Soal Tugas 14*/
	//<Tugas14 />
	//<Tugas14Skill />
	
	/*Soal Tugas 15*/
	//<Tugas15 />
	//<TugasNavigation />
	
	
	/* Quis 3*/
	<Quis3 />
	
	
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow', 
  },
});